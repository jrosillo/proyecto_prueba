
class Cuadrado:
	def __init__(self,base,altura):
		self.base = base
		self.altura = altura

	def calcSuperficie(self):
		return self.base*self.altura

	def getBase(self):
		return self.base

	def setBase(self,base):
		if (type(base) == int or type(base) == float) or base.isdigit():
			self.base = float(base)
		else:
			print ("Eso no es un número o es menor que 0")


	def getAltura(self):
		return self.altura

	def setAltura(self,altura):
		if (type(altura) == int or type(altura) == float) or altura.isdigit():
			self.altura = float(altura)
		else:
			print ("Eso no es un número o es menor que 0")

class Triangulo:
	def __init__(self,base,altura):
		self.__base = base
		self.__altura = altura

	def calcSuperficie(self):
		return (self.__base*self.__altura)/2

	def getBase(self):
		return self.__base

	def getBase(self):
		return self.__base

	def setBase(self,base):
		if (type(base) == int or type(base) == float) or base.isdigit():
			self.__base = float(base)
		else:
			print ("Eso no es un número o es menor que 0")

	def getAltura(self):
		return self.__altura

	def setAltura(self,altura):
		if (type(altura) == int or type(altura) == float) or altura.isdigit():
			self.__altura = float(altura)
		else:
			print ("Eso no es un número o es menor que 0")

class Circulo:
	def __init__(self,radio):
		self.__radio = radio

	def calcSuperficie(self):
		return math.pi*(self.__radio**2)

	def getRadio(self):
		return self.__radio

	def setBase(self,radio):
		if (type(radio) == int or type(radio) == float) or radio.isdigit():
			self.__radio = float(radio)
		else:
			print ("Eso no es un número o es menor que 0")



#print(circulo.getRadio())
